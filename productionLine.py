"""
TPK4186 - Assignment 3
Made by: The nuts and bolts bros (Sami Sayeed & Sigurd Humerfelt)
Class: ProductionLine - cleaned
"""

import random

from batch import Batch
from buffer import Buffer
from task import Task
from wafer import Wafer
from unit import Unit

PROCESSING_TIME: list[float] = [0.5, 3.5, 1.2, 3, 0.8, 0.5, 1, 1.9, 0.3]

class ProductionLine:
    PRODUCTION_OF_WAFERS = 1000
    BATCHES = 0
    strResults = []
    
    def __init__(self, batch_size: int = 20, batch_optimization: bool = False):
        self.__usedTime : int = 0
        self.__tasks : list[Task] = []
        self.__buffers : list[Buffer] = []
        self.__units : list[Unit] = []
        self.__batches : list[Batch] = []
        self.BATCH_SIZE = batch_size
        self.batch_loading_optimization = batch_optimization
        self.chosenAlgo = 0

    def setChosenAlgo(self, algo: int) -> None:
        self.chosenAlgo = algo

    def produceBatch(self) -> None:
        if self.batch_loading_optimization == False:
            self.generateBatchesAtStart(self.BATCH_SIZE)
            return
        if self.BATCHES >= self.PRODUCTION_OF_WAFERS:
            return
        bat = Batch()
        ant = self.BATCH_SIZE
        #bat.generateBatch(ant if (self.BATCHES + ant) <= self.PRODUCTION_OF_WAFERS else (self.PRODUCTION_OF_WAFERS - self.BATCHES))  # For å få den til akkurat 1000
        bat.generateBatch(ant)
        self.addBatch(bat)
        self.BATCHES += ant
 
    def generateTasksAndBuffers(self) -> None:
        y = 0
        for tall in PROCESSING_TIME:
            tasken = Task(len(self.getTasks()) + 1, tall)
            if y > 0:
                tasken.setInputBuffer(self.getTasks()[len(self.getTasks()) - 1].getOutputBuffer())
                tasken.setOutputBuffer(Buffer())
                self.addTask(tasken)
            else:
                buffen = Buffer(len(self.getBuffers()) + 1)
                buffen1 = Buffer(len(self.getBuffers()) + 100)

                self.addBuffer(buffen)
                self.addBuffer(buffen1)

                tasken.setInputBuffer(buffen)
                tasken.setOutputBuffer(buffen1)

                self.addTask(tasken)
                y += 1
    
    def generateUnits(self) -> None:
        self.addUnit(Unit(len(self.getUnits()) + 1, [self.getTask(0), self.getTask(2), self.getTask(5), self.getTask(8)]))
        self.addUnit(Unit(len(self.getUnits()) + 1, [self.getTask(1), self.getTask(4), self.getTask(6)]))
        self.addUnit(Unit(len(self.getUnits()) + 1, [self.getTask(3), self.getTask(7)]))

    # generates all batches into a "bucket" before the productionLine (This is task 5)
    def generateBatchesAtStart(self, ant: int = -1) -> None:
        currentSize = 0
        while currentSize < self.PRODUCTION_OF_WAFERS:
            bat = Batch()
            #bat.generateBatch(ant if (currentSize + ant) <= self.PRODUCTION_OF_WAFERS else (self.PRODUCTION_OF_WAFERS - currentSize)) #For å få den til akkurat 1000
            bat.generateBatch(ant)
            self.addBatch(bat)
            currentSize += bat.getWaferListLength()

    def getUnit(self, index: int) -> Unit:
        return self.__units[index]
    
    def getUnits(self):
        return self.__units
    
    def addUnit(self, unit: Unit) -> None:
        self.__units.append(unit)

    def getBuffers(self):
        return self.__buffers
    
    def addBuffer(self, buffer: Buffer) -> None:
        self.__buffers.append(buffer)

    def getTask(self, index: int) -> Task:
        return self.__tasks[index]

    def addTime(self, time: float):
        self.__usedTime += time

    def getTime(self):
        return self.__usedTime
            
    def getBatches(self) -> list[Batch]:
        return self.__batches
    
    def getBatchesLength(self) -> int:
        return len(self.__batches)
    
    def addBatch(self, batch: Batch) -> None:
        self.__batches.append(batch)

    def removeBatch(self, batch: Batch) -> None:
        self.__batches.remove(batch)

    def getTasks(self):
        return self.__tasks
    
    def printTasks(self) -> str:
        strReturn = ""
        for task in self.__tasks:
            strReturn += "\n\t" + str(task)
        return strReturn

    def getStrResults(self) -> str:
        strReturn = ""
        for res in self.strResults[:-2]:
            strReturn += res + "\n\n"
        strReturn += self.strResults[-1]
        return strReturn
    
    def printBatchLoadingOptimization(self) -> str:
        if self.batch_loading_optimization == True:
            return "Batch loading optimization: ON " + str(self.getUnits()[self.unitToWaitOn])
        else:
            return "Batch loading optimization: OFF"

    def addTask(self, task: Task) -> None:
        self.__tasks.append(task)

    def __str__(self) -> str:
        if self.getTasks()[-1].getOutputBuffer().getBestBatch() is None:
            return f"Actual total time: {int(self.__usedTime)} s ({self.getTasks()[-1].getOutputBuffer().getBatchListLength()} batches) ({self.getTasks()[-1].getOutputBuffer().getAmountOfWafers()} wafers in last buffer) ({self.printBatchLoadingOptimization()}) {self.printTasks()}"
        return f"Actual total time: {int(self.__usedTime)} s ({self.getTasks()[-1].getOutputBuffer().getFirstBatch().getWaferListLength()} wafers) ({self.getTasks()[-1].getOutputBuffer().getBatchListLength()} batches) ({self.getTasks()[-1].getOutputBuffer().getAmountOfWafers()} wafers in last buffer) ({self.printBatchLoadingOptimization()}) {self.printTasks()}"

    def __repr__(self) -> str:
        return self.__str__()


if __name__ == "__main__":
    pass