"""
TPK4186 - Assignment 3
Made by: The nuts and bolts bros (Sami Sayeed & Sigurd Humerfelt)
Class: Buffer - cleaned
"""


from __future__ import annotations
import math
from batch import Batch
from wafer import Wafer

class Buffer:
    MAX_WAFERS = 120

    def __init__(self, iden: int = 0):
        self.__id = id(self)
        self.__batchList : list[Batch] = []
    
    def setAsLastBuffer(self):
        self.MAX_WAFERS = math.inf
    
    def setBatch(self, batch: Batch):
        self.__batch = batch

    def setBatchList(self, batchList: list[Batch]):
        self.__batchList = batchList

    def removeBatch(self, batch: Batch):
        self.__batchList.remove(batch)
    
    
    def addBatch(self, batch: Batch):
        if batch is None:
            print("Batch is None, cannot add batch")
            return
        if self.getAmountOfWafers() + batch.getWaferListLength() > self.MAX_WAFERS:
            print(self.getID() , "is full, cannot add batch")
            print("Buffer is full, cannot add batch")
            return
        else:
            self.__batchList.append(batch)

    def getAmountOfWafers(self):
        wafers = 0
        for batch in self.__batchList:
            wafers += batch.getWaferListLength()
        return wafers
    
    def hasSpaceForBatch(self, batch: Batch) -> bool:
        return self.getAmountOfWafers() + batch.getWaferListLength() <= self.MAX_WAFERS
    
    def hasBatch(self) -> bool:
        return len(self.__batchList) > 0
    
    def isAvailable(self) -> bool:
        return self.getAmountOfWafers() < self.MAX_WAFERS 

    def getFirstBatch(self) -> Batch: 
        try:
            return self.__batchList[0]
        except IndexError:
            return None
        
    def getBestBatch(self) -> Batch:
        if len(self.getBatchList()) == 0:
            return None
        return sorted(self.getBatchList(), key=lambda x: x.getWaferListLength())[0]
    
    def getBatchList(self) -> list[Batch]:
        return self.__batchList
    
    def getBatchListLength(self) -> int:
        return len(self.__batchList)
    
    def getAmountOfWafers(self) -> int:
        wafers = 0
        for batch in self.__batchList:
            wafers += batch.getWaferListLength()
        return wafers
    
    def getLimit(self) -> int:
        return self.MAX_WAFERS
    
    def getID(self) :
        return self.__id
    
    def __str__(self) -> str:
        return " ¡Buffer" + str(self.getID())[-3:] + " <" + str(self.getBatchList()) + ">cap"+ str(self.MAX_WAFERS) +"!"

    def __repr__(self) -> str:
        return self.__str__()
    



if __name__ == "__main__":
    pass
