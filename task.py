"""
TPK4186 - Assignment 3
Made by: The nuts and bolts bros (Sami Sayeed & Sigurd Humerfelt)
Class: Task cleaned
"""
import math
from batch import Batch
from buffer import Buffer


class Task:
    time = 0.0
    LOAD_TIME = 1

    def __init__(self, id: int, processingTime: float):
        self.__id = id
        self.__inputBuffer = None
        self.__outputBuffer = None
        self.__processingTime = processingTime
        self.__state = None

    def setInputBuffer(self, buffer: Buffer) -> None:
        self.__inputBuffer = buffer

    def setOutputBuffer(self, buffer: Buffer) -> None:
        self.__outputBuffer = buffer
    
    def completeBatch(self, time : int) -> None:
        
        if not self.isAvalible():
            raise Exception("Task is not avalible")
        if self.getOutputBuffer() == None:
            self.setOutputBuffer(Buffer(self.__id + 10))
        if not self.getOutputBuffer().isAvailable():
            print(self.getOutputBuffer() , "already has a batch, can't add another one")
            return
        self.__state = "Occupied"
        buffer = self.getInputBuffer()
        batch = buffer.getBestBatch()
        
        self.getOutputBuffer().addBatch(batch)
        self.getInputBuffer().removeBatch(batch)
        
        self.__state = None
        self.time += batch.getWaferListLength() * self.__processingTime + self.LOAD_TIME * 2

    def isAvalible(self) -> bool:
        return self.getState() == None
    
    def getState(self) -> str:
        return self.__state
    
    def getStateAsStr(self) -> str:
        if self.getState() == None:
            return "Avalible"
        return self.getState()
    
    def getTime(self) -> float: #this is the active time for the task
        return self.time
    
    def getTotalTime(self) -> float:
        return self.time + self.__processingTime * self.getInputBuffer()

    def getProcessingTime(self) -> float:
        return self.__processingTime
    
    def getBatchProcessingTime(self) -> float:
        if self.getInputBuffer().getBatchListLength() < 1:
            return -1
        return self.__processingTime * self.getInputBuffer().getBestBatch().getWaferListLength() + self.LOAD_TIME * 2

    def getOutputBuffer(self) -> Buffer:
        return self.__outputBuffer
    
    def getInputBuffer(self) -> Buffer:
        return self.__inputBuffer
    
    def getAmountOfWafers(self) -> int:
        return sum(batch.getWaferListLength() for batch in self.__outputBuffer.getBatchList())
    
    def getAmountOfWafersIn(self) -> int:
        return sum(batch.getWaferListLength() for batch in self.__inputBuffer.getBatchList())
    
    def getAmountOfWafersTotal(self) -> int:
        if self.getOutputBuffer().getLimit == math.inf:
            return self.getAmountOfWafersIn()
        return self.getAmountOfWafers() + self.getAmountOfWafersIn()        
    
    def getID(self) -> int:
        return self.__id
    
    def __str__(self) -> str:
        return "Task" + str(self.__id) + " after " + str(int(self.getTime())) + "\tin " + str(self.getAmountOfWafersIn()) + " of " + str(self.__inputBuffer.MAX_WAFERS) + "\tout " + str(self.getAmountOfWafers()) + " of " + str(self.__outputBuffer.MAX_WAFERS) + "\twith processing time: " + str(self.getProcessingTime())
    
    def __repr__(self) -> str:
        return self.__str__()

if __name__ == "__main__":
    pass
