"""
TPK4186 - Assignment 3
Made by: The nuts and bolts bros (Sami Sayeed & Sigurd Humerfelt)
Class: Wafer Writer to write to latex file to generate pdf - cleaned
"""

from pylatex import Document, Section, Subsection, Tabular, Math, TikZ, Axis, \
    Plot, Figure, Matrix, Alignat, Document, LongTabu, HFill, LineBreak, Label, Ref, Document, Section, Subsection, Command, Enumerate, Hyperref, Package, Itemize
from pylatex.utils import italic, bold, italic, NoEscape, verbatim, escape_latex
from matplotlib import pyplot as plt

from productionLine import ProductionLine
from simulator import Simulator



AMOUNT_OF_BATCHES = 5

EXPORT_PDF_NAME = "TPK4186_rapport_WaferWaffles"
EXPORT_FILE_NAME = "developmentOfProductionLinen"

class WaferWriter:
    # constructor
    def __init__(self, name: str = "WaferWaffles"):
        self.name = name
        self.sim = Simulator()
    
    def makeRunTimeGraph(self, doc) -> None:
        with doc.create(Subsection("Execution time for different batch sizes")):
            doc.append("Here is plots of the execution time for different batch sizes (either with ~1000 wafers or a max of 5 batches). The x-axis is the batch size, and the y-axis is the execution time.")

            results = self.makeGraph(doc)
            self.makeGraph(doc, AMOUNT_OF_BATCHES)
            self.makeGraph(doc, AMOUNT_OF_BATCHES/AMOUNT_OF_BATCHES)
            with doc.create(Subsection("This is the best result: ")):
                minValue = min(results, key=lambda x: x.getTime())
                minList = [x for x in results if x.getTime() == minValue.getTime()]
                self.minPLlist = minList
                for result in minList:
                    doc.append(LineBreak())
                    doc.append(result)
            
                
    def makeGraph(self, doc, amount_of_batches: int = -1) -> None:
        algo = [0, 0, 1]
        bo = [True, False, True]
        res = [[], [], []]
        results = []
        for i in range(20, 51):
            for y in range(len(bo)):
                pl = ProductionLine(i, bo[y])
                pl.setChosenAlgo(algo[y])
                self.sim.setUp(y % 3, pl, amount_of_batches)
                res[y].append(pl.getTime())
                results.append(pl)

        for i in range(0, len(res)):
            if i == len(res) - 1:
                plt.plot(range(20, 51), res[i],
                            label="Random selection of tasks")
            else:
                strengen = "With optimization on " + \
                    str(pl.getUnits()[i]) if bo[i] else "No optimization"
                plt.plot(range(20, 51), res[i], label=strengen)
        plt.legend()
        
        with doc.create(Figure(position='!htbp')) as plot:
            plot.add_plot(width=NoEscape(300))
            strInfo = "at least " + str(results[0].PRODUCTION_OF_WAFERS) + ' wafers' if amount_of_batches == -1 else str(int(amount_of_batches)) + ' batch(es)'
            plot.add_caption('Distrobution of the time to produce ' + strInfo)
        plt.close()
        return results


    
    def makeSixYearOldsUnderstand(self, doc) -> None:
        with doc.create(Section("How do I explain this assignment to a six year old?")):
            doc.append("Holy Santa Clauss, this Assignment was hard to understand. But when I \"translated\" it into something I had a better understand of, I was able to understand the basics. So here I will use my own words (and car analogy) to explain this assignment:")
            self.makeSpacing(doc)
            doc.append("Suppose you are Karl Benz, and you want to produce 1000 Mercedes Benz cars. You have made a factory with 3 types of machines; one spesialized for tires, one for motor and one for bodywork. ")
            self.makeSpacing(doc)
            doc.append("And each machine has multiple tasks to perform, ie the motor machine has tasks like \"putting on the motor\", \"putting on the exhaust\", \"putting on the radiator\" and so on. In addition, will each task take a specific amount of time to perform (it is more time consuming to connect the motor than to add windshield wipers).")
            self.makeSpacing(doc)
            doc.append("For a car to be built you need to perform all the tasks in a specific order. For example, you can't put on the radiator before you have put on the motor.")
            self.makeSpacing(doc)
            doc.append("As it specialised machines for tier, bodywork and motor, each machine can only perform one task at a time. So you need to have a system to manage what machine will work at the optimal time.")
            self.makeSpacing(doc)
            doc.append(
                "This was my basic understanding of this assignment. And I think it is a good way to explain it so a six year old can understand.")
            
    def makeOurWork(self, doc) -> None:
        with doc.create(Section("What we did, and how we did it")):
            doc.append("Task 1 resulted in the classes `Wafer`, `Batch`, `Buffer`, `Task`, `Unit` and `ProductionLine`. We also made a `__str__` and `__repr__` method in each class to make it easier to print out the objects, as previously mentioned.")
    
    def makeResults(self, doc) -> None:
        with doc.create(Section("The results of what we did, and our thoughts about it")): # TODO not hard code the results
            doc.append("We think we did a good job with this assignment, considering the complexety and the degree of difficulty. We are not super happy with the results, but we did at least get an answer to show for, and implemnted an optimazation that reduced the time a little bit. ")
        self.makeRunTimeGraph(doc)
    
    def makeExplanation(self, doc) -> None:
        with doc.create(Section("But how does the code work?")):
            doc.append("We have made 8 classes for this assignment. They are:")
            with doc.create(Enumerate(enumeration_symbol=r"\arabic*",
                                      options={'start': 1})) as enum:
                enum.add_item("`Wafer` class. This is somewhat a pretty barebone class made to represent a wafer. It has some constants, but those are not used.")
                enum.add_item(
                    "`Batch` class. This class is used to represent a batch (or list if you like) of wafers. The list is a list of `Wafer` objects with minimum and maximum constraints (20 to 50).")
                enum.add_item("`Buffer` class. This class is used to represent a buffer (or a waiting room if you like). It has a list of `Batch` objects, and a maximum size. It also has a method to add a batch to the buffer, and a method to remove a batch from the buffer. Each output buffer is the input buffer of the next task, if it has a next task.")
                enum.add_item("`Task` class. This class is used to represent a task (or action if you want). It has a reference to the input buffer and the output buffer. It also has a method to perform the task: Check if it has space in outputbuffer, add time it takes, and unload it onto the output buffer. It also has the prosessing time as it takes for one wafer to go trough. This class is the representation of the Action clas. In a addition to a constant to represent the load time of a wafer into the input or output buffer, if that should change.")
                enum.add_item("`Unit` class. This class is used to represent the unit (or the same category of machines). It has a list of `Task` objects to represent the tasks it can perform. It also has a constant to represent the amount of active tasks a machine can have at the same time, if that would change in the future.")
                enum.add_item("`ProductionLine` class. This contains all the refrences to the different parts of the system, as well as a printer for each result") # TODO skriv mer her
                enum.add_item("`Simulator` class. This is the holy  grail of this assignment. This contains the simulation loop for the `ProductionLine` as well as a scheduler. We did not see a need to make a seperate scheduler, because we to into acount the estimated time. This class also conatins a method to make a file of given simulations as in task 4.")
                enum.add_item("`WaferWriter` class. This is too generate this rapport as a latex document. This is the same style as Assignment 3, and the corresponding dependencies are the same.") 
            doc.append("Now you might say: \"Oooh, shot you forgot the printer class \". We choose to not make a printer class, but instead we have a `__str__` and `__repr__` method in each class. This is because we wanted to print out the objects in a nice way, as we did not want to make a printer class. So we just made a `__str__` and `__repr__` method in each class. If you want to read more about that, visit") 
            doc.append(self.hyperlink("https://www.digitalocean.com/community/tutorials/python-str-repr-functions", "this link."))
            self.makeSpacing(doc)
            doc.append(
                "To run the files (if you want to generate this pdf you need to run the `WaferWriter` class) need to at least have a latex compiler and the pylatex, pandas and matplot python libraries installed.")

    def makeAssumptions(self, doc) -> None:
        with doc.create(Section("Assumptions and clarifications")):
            doc.append("We have made some assumptions and clarifications for this assignment. They are:")

            with doc.create(Itemize()) as enum:
                enum.add_item("The time it takes to produce one task in one unit depends on the longest time the chosen task in the other unit takes. This assumption is far from optimal, but we did not manage to make a working system otherwise :(")
                enum.add_item("The last batch will always contain the chosen BATCH_SIZE. A consequence of this is that the total wafers produced could exceed the PRODUCTION_OF_WAFERS limit. We choose to respect the constraint of the different BATCH_SIZEs, and not the total amount of wafers produced. We could have solved this another way but this is our second assumption.")
                enum.add_item("The batches will always have the same amount of wafers relative to the other batches in the same productionLine. This is because we did see it most realistic that the batches would have the same amount of wafers.")
                enum.add_item("As a consequence of the previous assumption, our interpretation of Task 6 is that you need to find the optimal BATCH_SIZE")
                enum.add_item("The internal time in every task is the active time, inlcuding the time it takes to load and unload the wafers")
            doc.append("To run the main file (to generate this file), you need to run the 'waferWriter' class")
    
    def makeSpacing(self, doc) -> None:
        doc.append(LineBreak())
        doc.append(LineBreak())
    
    def hyperlink(self, url, text):
        text = escape_latex(text)
        return NoEscape(r'\href{' + url + '}{' + text + '}')
    
    def makeTask1(self, doc) -> None:
        with doc.create(Subsection("Task 1")):
            doc.append("Task 1 resulteted in the `Wafer`, `Buffer`, `Task`, Unit, and `ProductionLine` classes")
    
    def makeTask2(self, doc) -> None:
        with doc.create(Subsection("Task 2")):
            doc.append(
                "As previously mentioned was this task implemented with each class having their own str and repr function. In a addition, dose the ProductionLine contain a refrence to all the previous results of the line.")
            
    def makeTask3(self, doc) -> None:
        with doc.create(Subsection("Task 3")):
            doc.append(
                "Task 3 resulted in adding the `Simulator` class, and that we represent the `Task` class as the `Action`class")


    def makeTask4(self, doc) -> None:
        with doc.create(Subsection("Task 4")):
            doc.append("The text file for task 4 i genereated as " +
                       EXPORT_FILE_NAME + ".txt")
            self.makeSpacing(doc)
            doc.append("The rest of task 4 can be seen in this pdf, and shows graphs of the different scenarios with different limitations")
            self.makeSpacing(doc)
            doc.append("As shown in those graphs, is that our optimalization is not always an optimalization. Sadly does not we know why this is the case, and if we would understand we would have fixed it. On the other hand, will the best result be best with our optimalization. The results from 1 batch indicates that our optimalization is not wrong, but that it is not always the best.")
            s = Simulator()
            s.initializeProductionLineOfOne()
            s.initializeProductionLine(5)
            s.initializeProductionLine()
            s.printToFile(EXPORT_FILE_NAME + ".txt")

    def makeTask5(self, doc) -> None:
        with doc.create(Subsection("Task 5")):
            doc.append("The task 5 was a real headache, but we got a better best result than without it. The worst case scenario is to wait on the last task to finish a batch, and would give a really slow result. On the other hand is to wait on the first task to have space and ability to produce a batch. The production line has its own list of batches to produce, as the `__batches` list. This is so the first input buffer does not get overloaded.")
    
    def makeTask6(self, doc) -> None:
        with doc.create(Subsection("Task 6")):
            doc.append(
                "The ordering heuristics is the `getChosenHeuristic` function in the `Unit` class. We did multiple experimental calculations, in addition to have a random ordering heuristic. These calculations can be seen in this table, be noted that this graph is hardcoded. If you would like to change these parameters, you can change the ariables `a`, `b` and `c` in the `Unit` class, and run the 'waferWriter' class to generate this pdf once more. As expected, was our heuristic better than the random ordering, but did not give results near the professors record of 5673 time units, but that is probaly why he is a professor and we are not :D")
            self.makeSpacing(doc)
            with doc.create(Tabular('|p{3cm}|p{3cm}|')) as table:
                table.add_hline()
                table.add_row(bold(
                    'a, b & c values'), bold('Best result'))
                table.add_hline()
                table.add_row([0.48, 1, 1], 6337)
                table.add_hline()
                table.add_row([1, 1, 1], 6352)
                table.add_hline()
                table.add_row([1, 1, 1.5], 6502)
                table.add_hline()
                table.add_row([1, 1.5, 1.5], 6500)
                table.add_hline()
            
    
    def makeTask7(self, doc) -> None:
        with doc.create(Subsection("Task 7")):
            doc.append(
                "As the assumption previously mentioned did we se it most realistic to have a fixed BATCH_SIZE thorughout a production line. The result of this can be seen in the graphs, and show that the best result is with a BATCH_SIZE of " + str(self.minPLlist[0].BATCH_SIZE) +" with a processing time of " + str(self.minPLlist[0].getTime()) + " time units. The graph is shown earlier in this report")

    
    def makeExplanationOfEachTask(self, doc) -> None:
        with doc.create(Section("Explanations according to each task")):
            doc.append("This section contains all the explanations of the different tasks")
            tasks = [self.makeTask1, self.makeTask2, self.makeTask3, self.makeTask4, self.makeTask5, self.makeTask6, self.makeTask7]
            for funk in tasks:
                funk(doc)

    def makeDocument(self):
        doc = Document(documentclass='article', fontenc='', lmodern=True,
                       inputenc='utf8', page_numbers=True)

        doc.preamble.append(Command('title', 'TPK4186 rapport - Wafer Waffles'))
        doc.preamble.append(
            Command('author', 'Made by: the Nuts and Bolts Bros (aka Sami Sayeed & Sigurd Humerfelt)'))
        doc.preamble.append(Command('date', NoEscape(r'\today')))
        doc.packages.append(Package('hyperref'))
        doc.append(bold(NoEscape(r'\maketitle')))
        self.fill_document(doc)

        doc.generate_pdf(self.name)

    def fill_document(self, doc):
        self.makeSixYearOldsUnderstand(doc)
        self.makeAssumptions(doc)
        self.makeExplanation(doc)
        self.makeOurWork(doc)
        self.makeResults(doc)
        self.makeExplanationOfEachTask(doc)

if __name__ == '__main__':
   WaferWriter(EXPORT_PDF_NAME).makeDocument()
