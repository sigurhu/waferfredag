"""
TPK4186 - Assignment 3
Made by: The nuts and bolts bros (Sami Sayeed & Sigurd Humerfelt)
Class: Unit - cleaned
"""

from task import Task
import random

class Unit:
    def __init__(self, id: int = 0, tasks: list[Task] = []):
        self.__id: int = id
        self.__tasks: list[Task] = tasks

    def getTaskToExecute(self, chosen_algo: int = -1) -> Task:
        if chosen_algo == 1:
            return self.getRandomHeuristic()
        else:
            return self.getChosenHeuristic()

    def getRandomHeuristic(self) -> Task:
        task = self.getTasks()[random.randint(0, len(self.getTasks()) - 1)]  # random task delegering
        return task
    
    def getChosenHeuristic(self) -> Task:
        tasks = [x for _, x in sorted(zip(self.getProcessingTimes(), self.getTasks()), key=lambda x: x[0])]
        # det som gir  best resultat er id = 0.48 wafers = 1 processing time = 1
        a = 0.48
        b = 1
        c = 1
        task = sorted(tasks, key=lambda x: x.getID()**(a) * x.getInputBuffer().getAmountOfWafers()**(b) * x.getProcessingTime()**(c))[-1]
        return task

    def getProcessingTimes(self) -> list[float]:
        return [task.getProcessingTime() for task in self.__tasks]
    
    def getTask(self, index: int) -> Task:
        return self.__tasks[index]

    def getTasks(self):
        return self.__tasks

    def setTasks(self, tasks: list) -> None:
        self.__tasks = tasks

    def getTaskNames(self) -> str:
        names = ""
        for task in self.__tasks:
            names += str(task.getID()) + ", "
        return names 

    def __str__(self) -> str:
        return "Unit " + str(self.__id) + " with Task " + str(self.getTaskNames()[:-2])

    def __repr__(self) -> str:
        return self.__str__()


if __name__ == "__main__":
    pass
