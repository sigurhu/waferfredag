"""
TPK4186 - Assignment 3
Made by: The nuts and bolts bros (Sami Sayeed & Sigurd Humerfelt)
Class: Simulator - cleaned
"""
from buffer import Buffer
from productionLine import ProductionLine
from task import Task
from wafer import Wafer
import random
import math

class Simulator:
    BATCH_SIZE = 20

    def __init__(self):
        self.__productionLine = ProductionLine(batch_size=self.BATCH_SIZE, batch_optimization=True)

    def initializeProductionLineOfOne(self):
        self.__productionLine.PRODUCTION_OF_WAFERS = self.BATCH_SIZE
        self.__productionLine.generateTasksAndBuffers()
        self.__productionLine.getTasks()[-1].getOutputBuffer().setAsLastBuffer()
        self.__productionLine.generateUnits()
        self.__productionLine.produceBatch()
        self.simulationLoop(production_line=self.__productionLine)

    def initializeProductionLine(self, max_amount_of_batches: int = -1):
        for i in range(20, 51):  # per batch size possible
            pl = ProductionLine(
                    batch_size=i, batch_optimization=False)
            if max_amount_of_batches != -1:
                pl.PRODUCTION_OF_WAFERS = max_amount_of_batches * i
            pl.generateTasksAndBuffers()
            pl.getTasks()[-1].getOutputBuffer().setAsLastBuffer()
            pl.generateUnits()
            pl.produceBatch()
            self.simulationLoop(production_line=pl)

    def setUp(self, unitIndexToOptimize: int, production_line: ProductionLine,  max_amount_of_batches: int = -1) -> None:
        production_line.generateTasksAndBuffers()
        production_line.getTasks()[-1].getOutputBuffer().setAsLastBuffer()
        production_line.generateUnits()
        if max_amount_of_batches != -1:
            production_line.PRODUCTION_OF_WAFERS = max_amount_of_batches * production_line.BATCH_SIZE
        production_line.produceBatch()

        self.simulationLoop(unitIndexToOptimize, production_line=production_line)

    def simulationLoop(self, unitIndex: int = 0, production_line: ProductionLine = None) -> None:
        pl = production_line
        pl.unitToWaitOn = unitIndex
        tot_tid = 0
        while (pl.getBatchesLength() + sum(t.getAmountOfWafersTotal() for t in pl.getTasks()[:-1])) > 0:
            # This is task 5
            if len(pl.getBatches()) > 0 and pl.getTask(0).getInputBuffer().hasSpaceForBatch(pl.getBatches()[0]):
                pl.getTask(0).getInputBuffer().addBatch(pl.getBatches()[0])
                pl.removeBatch(pl.getBatches()[0])
            tasks_this_iteration = []
            for unit in pl.getUnits():
                # here is the algos for choosing task (representing task 6) TODO maybe move it into unit class
                tasks_this_iteration.append(unit.getTaskToExecute(pl.chosenAlgo))
            _time = max(task.getBatchProcessingTime()
                        for task in tasks_this_iteration)
            _time = max(task.getBatchProcessingTime() * (_time // task.getBatchProcessingTime()) +
                        task.LOAD_TIME * 2 * (_time // task.getBatchProcessingTime()) for task in tasks_this_iteration)

            tidenMedAvlastning = []
            for task in tasks_this_iteration:
                timeForAll = 0
                # TODO: må denne endres for å kunne brukes optimalt?
                for _ in range(0, int(_time // task.getBatchProcessingTime())):
                    timeForOne = task.getBatchProcessingTime()
                    if task.getInputBuffer().hasBatch() and task.getOutputBuffer().hasSpaceForBatch(task.getInputBuffer().getBestBatch()):
                        task.completeBatch(_time)
                        timeForAll += timeForOne
                        # Task 5 to reduce the minimal time
                        if (not task in pl.getUnits()[unitIndex].getTasks()) and pl.batch_loading_optimization == True:
                            pl.produceBatch()
                tidenMedAvlastning.append(timeForAll)
            tot_tid += max(tidenMedAvlastning)
            pl.strResults.append(str(pl))
        pl.addTime(tot_tid)
        pl.strResults.append(str(pl))
    
    
    def printToFile(self, filename: str) -> None:
        with open(filename, 'w') as f:
            f.write(self.__productionLine.getStrResults())

    
            
if __name__ == "__main__":
    s = Simulator()
    s.initializeProductionLineOfOne()
    s.initializeProductionLine(5)
    s.initializeProductionLine()
    s.printToFile("developmentOfProductionLinen.txt")
    print("Done")
    