"""
TPK4186 - Assignment 3
Made by: The nuts and bolts bros (Sami Sayeed & Sigurd Humerfelt)
Class: Batch (of wafers)
"""

from __future__ import annotations
from wafer import Wafer
import random

class Batch:
    MAX_BATCH_SIZE = 50
    MIN_BATCH_SIZE = 20

    def __init__(self, waferList: list[Wafer] = []):
        self.__wafers = waferList
    
    def generateBatch(self, size: int = -1) -> None:
        if size == -1:
            size = random.randint(self.MIN_BATCH_SIZE, self.MAX_BATCH_SIZE)
        if size < self.MIN_BATCH_SIZE:
            raise Exception("Batch size is too small for this batch")
        if size > self.MAX_BATCH_SIZE:
            raise Exception("Batch size is too large for this batch")
        self.__wafers = [Wafer(i) for i in range(size)]
    
    def getWaferListLength(self) -> int:
        if self.__wafers == [] or self.__wafers is None:
            return 0
        return len(self.__wafers)

    def __str__(self) -> str:
        return "Batch with " + str(len(self.__wafers)) + " wafers"
    
    def __repr__(self) -> str:
        return self.__str__()

if __name__ == "__main__":
    b = Batch()
    b.generateBatch(100)
    print(b)
